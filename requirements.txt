# This file may be used to create an environment using:
# $ conda create --name <env> --file <this file>
# platform: win-64
absl-py=0.8.0=pypi_0
astor=0.8.0=pypi_0
blas=1.0=mkl
ca-certificates=2019.1.23=0
certifi=2019.3.9=py36_0
cffi=1.12.2=py36h7a1dbc1_1
cudatoolkit=10.0.130=0
cycler=0.10.0=py36h009560c_0
ffmpeg=2.7.0=0
freetype=2.9.1=ha9979f8_1
gast=0.3.2=pypi_0
google-pasta=0.1.7=pypi_0
grpcio=1.23.0=pypi_0
h5py=2.10.0=pypi_0
hdf5=1.8.20=hac2f561_1
icc_rt=2019.0.0=h0cc432a_1
icu=58.2=ha66f8fd_1
intel-openmp=2019.1=144
jpeg=9c=hfa6e2cd_1001
keras-applications=1.0.8=pypi_0
keras-preprocessing=1.1.0=pypi_0
kiwisolver=1.0.1=py36h6538335_0
libopencv=3.4.2=h20b85fd_0
libpng=1.6.36=h2a8f88b_0
libtiff=4.0.10=hb898794_2
libwebp=1.0.2=hfa6e2cd_1
markdown=3.1.1=pypi_0
matplotlib=3.0.3=py36hc8f65d3_0
mkl=2019.1=144
mkl_fft=1.0.10=py36h14836fe_0
mkl_random=1.0.2=py36h343c172_0
ninja=1.8.2=py36he980bc4_1
numpy=1.16.2=py36h19fb1c0_0
numpy-base=1.16.2=py36hc3f5095_0
olefile=0.46=py36_0
opencv=3.4.2=py36h40b0b35_0
openssl=1.1.1b=he774522_1
pillow=5.4.1=py36hdc69c19_0
pip=19.0.3=py36_0
protobuf=3.7.1=pypi_0
py-opencv=3.4.2=py36hc319ecb_0
pycparser=2.19=py36_0
pyparsing=2.3.1=py36_0
pyqt=5.9.2=py36h6538335_2
python=3.6.8=h9f7ef89_7
python-dateutil=2.8.0=py36_0
pytorch=1.0.1=py3.6_cuda100_cudnn7_1
pytz=2018.9=py36_0
qt=5.9.7=vc14h73c81de_0
scikit-learn=0.20.3=py36h343c172_0
scipy=1.2.1=py36h29ff71c_0
setuptools=40.8.0=py36_0
sip=4.19.8=py36h6538335_0
six=1.12.0=py36_0
sqlite=3.27.2=he774522_0
tensorboard=1.14.0=pypi_0
tensorboardx=1.6=pypi_0
tensorflow=1.14.0=pypi_0
tensorflow-estimator=1.14.0=pypi_0
termcolor=1.1.0=pypi_0
thop=0.0.23-1907171430=pypi_0
tk=8.6.8=hfa6e2cd_0
torchvision=0.2.2=py_3
tornado=6.0.1=py36he774522_0
vc=14.1=h21ff451_3
vs2015_runtime=15.5.2=3
werkzeug=0.16.0=pypi_0
wheel=0.33.1=py36_0
wincertstore=0.2=py36h7fe50ca_0
wrapt=1.11.2=pypi_0
xz=5.2.4=h2fa13f4_4
zlib=1.2.11=h62dcd97_3
zstd=1.3.7=h508b16e_0
